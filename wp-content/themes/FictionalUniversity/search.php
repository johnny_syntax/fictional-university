<?php get_header(); ?>

<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/ocean.jpg'); ?>);"></div>
  <div class="page-banner__content container container--narrow">
    <h1 class="page-banner__title"> Search Results </h1>
    <div class="page-banner__intro">
      <p> You searched for "<?php echo get_search_query(); ?>" </p>
    </div>
  </div>
</div>

<div class="container container--narrow page-section">
  <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('template-parts/content', get_post_type()) ?>
    <?php endwhile; ?>

    <?php echo paginate_links(); ?>
  <? else : ?>
    <h2 class="headline headline--small-plus"> No results matched that search. </h2>
    <?php get_search_form(); ?>
  <?php endif; ?>
</div>

<?php get_footer();?>
