<?php

require get_theme_file_path('/includes/search-route.php');

function university_files() {
    // queue scripts: use microtime() in place of version nuber to avoid caching during development
  wp_enqueue_script('main-university-js', get_theme_file_uri('/js/scripts-bundled.js'), null, microtime(), true);
  wp_enqueue_script( 'jquery-3.5.1.min',  get_theme_file_uri('/js/jquery-3.5.1.min.js'), null, microtime(), true);
	wp_enqueue_script( 'script-search',  get_theme_file_uri('/js/script-search.js'),  null, microtime(), true);

    // queue stylesheets: use microtime() in place of version nuber to avoid caching during development
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  wp_enqueue_style('university_main_styles', get_stylesheet_uri(), null, microtime());
    // set a global variable for a dynamic the root url: used by search script, view page source to see <![CDATA[ var universityData = {"root_url":"http:\/\/localhost:8000"}; ]]>
  wp_localize_script('main-university-js', 'universityData', array(
    'root_url' => get_site_url()
  ));
}

add_action('wp_enqueue_scripts', 'university_files');


function university_features() {
    // add support for dynamic header title
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'university_features');


function university_adjust_queries($query) {

  if (!is_admin() AND is_post_type_archive('event') AND $query->is_main_query() AND $query->is_main_query()) {
    $today = date('Y-m-d');
    $query->set('meta_key', 'event_date');
    $query->set('orderby', 'meta_value');
    $query->set('order', 'ASC');
    $query->set('meta_query', array(
      array(
        'key' => 'event_date',
        'compare' => '>=',
        'value' => $today
      )
    ));
  }
}

add_action('pre_get_posts', 'university_adjust_queries');


  // function adds a custom REST API field for author name
function university_custom_rest() {
  register_rest_field('post', 'authorName', array(
    'get_callback' => function() { return get_the_author(); }
  ));
}

add_action('rest_api_init', 'university_custom_rest');
