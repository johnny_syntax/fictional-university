<?php

  //
  // register rest route
  //

add_action('rest_api_init', 'universityRegisterSearch');

function universityRegisterSearch() {
  register_rest_route('university/v1', 'search', array(
    'methods' => WP_REST_SERVER::READABLE,
    'callback' => 'universitySearchResults'
  ));
}

function universitySearchResults($data) {
    // query by post type, forming an array, setting 's' to the URL search term.
  $mainQuery = new WP_Query(array(
    'post_type' => array('post', 'page', 'event'),
    's' => sanitize_text_field($data['term'])
  ));

    // initialize results array
  $results = array(
    'generalInfo' => array(),
    'events' => array()
  );

    // populate results array depending on post type
  while ($mainQuery->have_posts()) {
    $mainQuery->the_post();

    if (get_post_type() == 'post' OR get_post_type() == 'page' ) {
      array_push($results['generalInfo'], array(
        'title' => get_the_title(),
        'permalink' => get_the_permalink(),
        'postType' => get_post_type(),
        'authorName' => get_the_author(),
        // 'category' => get_the_category()
      ));
    }
    // // block separates pages from posts and determines category name for posts
    // if (get_post_type() == 'page' ) {
    //   array_push($results['generalInfo'], array(
    //     'title' => get_the_title(),
    //     'permalink' => get_the_permalink(),
    //     'postType' => get_post_type()
    //   ));
    // }
    //
    // if (get_post_type() == 'post' ) {
    //   $category = get_the_category();
    //   $categoryName = $category[0]->name;
    //   array_push($results['generalInfo'], array(
    //     'title' => get_the_title(),
    //     'permalink' => get_the_permalink(),
    //     'postType' => get_post_type(),
    //     'authorName' => get_the_author(),
    //     'category' => $categoryName
    //   ));
    // }

    if (get_post_type() == 'event') {
        // format date for desired display
      $eventMonth = date( 'M', strtotime(get_field('event_date')));
      $eventDay = date( 'd', strtotime(get_field('event_date')));
        // format description for desired display
      $description = null;
      if (has_excerpt()) {
        $description = get_the_excerpt();
      } else {
        $description = wp_trim_words(get_the_content(), 18);
      }

      array_push($results['events'], array(
        'title' => get_the_title(),
        'permalink' => get_the_permalink(),
        'month' => $eventMonth,
        'day' => $eventDay,
        'description' => $description
      ));
    }
  }

    // return results array
  return $results;
}
