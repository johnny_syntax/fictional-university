$(document).ready(function() {

    //
    // search functions
    //

    // on load, append overlay HMTL if javascript is enabled
  var body = ('body');
  var addSearchHTML = function () {
    $(body).append(`
      <div class="search-overlay">
        <div class="search-overlay__top">
          <div class="container">
            <i class="fa fa-search search-overlay__icon aria-hidden="true"></i>
            <input type="text" class="search-term" placeholder="What are you looking for?" id="search-term">
            <i class="fa fa-window-close search-overlay__close aria-hidden="true"></i>
          </div>
        </div>

        <div class="container">
          <div id="search-overlay__results"></div>
        </div>
      </div>
    `);
  }
  addSearchHTML();

    // set vars/ prefetch elements
  var openButton = $('.js-search-trigger'),
      closeButton = $('.search-overlay__close'),
      searchOverlay = $('.search-overlay'),
      searchField = $('#search-term'),
      resultsDiv = $('#search-overlay__results'),
      isOverlayOpen = false,
      isSpinnerVisible = false,
      previousValue,
      typingTimer;

    // on click, show or hide overlay
  $(openButton).on('click', function (e) {
    openOverlay();
      // disable traditional href search if js enabled
    e.preventDefault();
  });
  $(closeButton).on('click', function (e) {
    closeOverlay();
      // disable traditional href search if js enabled
    e.preventDefault();
  });

    // on call, show or hide overlay
  var openOverlay = function () {
    $(searchOverlay).addClass('search-overlay--active');
    $(body).addClass('body-no-scroll');
      // clears the search field and div incase of previous search
    searchField.val('');
    resultsDiv.html('');
      // focuses the input field without needing to click
    setTimeout(() => searchField.focus(), 301);
    isOverlayOpen = true;
  }
  var closeOverlay = function () {
    $(searchOverlay).removeClass('search-overlay--active');
    $(body).removeClass('body-no-scroll');
    isOverlayOpen = false;
  }

    // on call, shows loading animation, sets timer, resetting timer if it has been set, modifies overlay div with results after typing stops (accoring to timeout milliseconds)
  var typingLogic = function () {
      // only show loading animation if alpha-numeric keys are being pressed (or search string is changes: i.e. when not pressing left or right keys, etc. )
    if (searchField.val() != previousValue) {
      clearTimeout(typingTimer);
        // only show loading animation or results if there are values in the input field
      if (searchField.val()) {
          // if not visible, show loading animation (prevents reloading after evey keypress)
        if (!isSpinnerVisible) {
          resultsDiv.html('<div class="spinner-loader"><div>')
          isSpinnerVisible = true;
        }
        typingTimer = setTimeout(function() {
          // console.log('hello from timeout typing logic');
          getResults();
        }, 500);
      } else {
        resultsDiv.html('')
        isSpinnerVisible = true;
      }
    }
    previousValue = searchField.val();
  }
    // on call, set overlay div with search results
  var getResults = function () {
    $.getJSON(universityData.root_url + '/wp-json/university/v1/search?term=' + searchField.val(), (results) => {
      resultsDiv.html(`
        <div class="row">
          <div class="one-half">
            <h2 class="search-overlay__section-title"> General Information </h2>
            ${results.generalInfo.length ? '<ul class="link-list min-list">' : '<p> No general information matches that search. </p>'}
              ${results.generalInfo.map(item => `<li> <a href="${item.permalink}"> ${item.title} ${item.postType == 'post' ? `by ${item.authorName}` : ''} </a> </li>`).join('')}
            ${results.generalInfo.length ? '</ul>' : ''}
          </div>
          <div class="one-half">
            <h2 class="search-overlay__section-title"> Events </h2>
            ${results.events.length ? '' : '<p> No events match that search. <a href="${universityData.root_url}/events">View all events</a> </p>'}
            ${results.events.map(item => `
              <div class="post-item">
                <div class="event-summary">
                  <a class="event-summary__date t-center" href="${item.permalink}">
                    <span class="event-summary__month">${item.month}</span>
                    <span class="event-summary__day">${item.day}</span>
                  </a>
                  <div class="event-summary__content">
                    <h5 class="event-summary__title headline headline--tiny">
                      <a href="${item.permalink}"> ${item.title} </a>
                    </h5>
                    <p> ${item.description} <a href="${item.permalink}" class="nu gray"> View Event &raquo; </a> </p>
                  </div>
                </div>
              </div>
            `).join('')}
          </div>
        </div>
      `);
      isSpinnerVisible = false;
    });
  }

    //
    // keypress functions
    //

    // document-wide keydown functions
  $(document).keydown(function(e) {
    //   // logs which key is being pressed
    // console.log(e.keyCode);
      // on 's' key press, show overlay if not active, and not typing in site input fields
    if ((e.keyCode == 83) && (!isOverlayOpen) && !$('input, textarea').is(':focus')) {
      openOverlay();
    }
      // on 'esc' key press, hide overlay if active
    if ((e.keyCode == 27) && (isOverlayOpen)) {
      closeOverlay();
    }
  });

    // search input keydown functions
  $(searchField).keyup(function(e) {
    typingLogic();
  });
});
