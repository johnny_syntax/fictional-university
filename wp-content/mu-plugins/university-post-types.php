<?php

  // define custom post types
function university_post_types() {

    // 'event' post type labels
  $labels = array(
    'name'            => 'Events',
    'singular_name'   => 'Event',
    'archives'        => 'Event Archives',
    'attributes'      => 'Event Attributes',
    'all_items'       => 'All Events',
    'add_new_item'    => 'Add New Event',
    'add_new'         => 'Add New',
    'new_item'        => 'New Event',
    'edit_item'       => 'Edit Event',
    'update_item'     => 'Update Event',
    'view_item'       => 'View Event',
    'view_items'      => 'View Events',
    'search_items'    => 'Search Events'
  );
    // 'event' post type arguments
  $args = array(
    'labels'        => $labels,
    'public'        => true,
    'rewrite'       => array('slug' => 'events'),
    'has_archive'   => true,
    'supports'      => array('title', 'editor', 'excerpt'),
    'menu_icon'     => 'dashicons-calendar'
  );
    // register post type 'event'
  register_post_type('event', $args);

}

add_action('init', 'university_post_types');


// // Register Custom Post Type: for template see https://generatewp.com/post-type/
// function custom_post_type() {
//
// 	$labels = array(
// 		'name'                  => _x( 'Events', 'Post Type General Name', 'text_domain' ),
// 		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
// 		'menu_name'             => __( 'Post Types', 'text_domain' ),
// 		'name_admin_bar'        => __( 'Post Type', 'text_domain' ),
// 		'archives'              => __( 'Item Archives', 'text_domain' ),
// 		'attributes'            => __( 'Item Attributes', 'text_domain' ),
// 		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
// 		'all_items'             => __( 'All Items', 'text_domain' ),
// 		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
// 		'add_new'               => __( 'Add New', 'text_domain' ),
// 		'new_item'              => __( 'New Item', 'text_domain' ),
// 		'edit_item'             => __( 'Edit Item', 'text_domain' ),
// 		'update_item'           => __( 'Update Item', 'text_domain' ),
// 		'view_item'             => __( 'View Item', 'text_domain' ),
// 		'view_items'            => __( 'View Items', 'text_domain' ),
// 		'search_items'          => __( 'Search Item', 'text_domain' ),
// 		'not_found'             => __( 'Not found', 'text_domain' ),
// 		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
// 		'featured_image'        => __( 'Featured Image', 'text_domain' ),
// 		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
// 		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
// 		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
// 		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
// 		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
// 		'items_list'            => __( 'Items list', 'text_domain' ),
// 		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
// 		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
// 	);
// 	$args = array(
// 		'label'                 => __( 'Event', 'text_domain' ),
// 		'description'           => __( 'University Events', 'text_domain' ),
// 		'labels'                => $labels,
// 		'supports'              => array( 'title', 'editor' ),
// 		'taxonomies'            => array( 'category', 'post_tag' ),
// 		'hierarchical'          => false,
// 		'public'                => true,
// 		'show_ui'               => true,
// 		'show_in_menu'          => true,
// 		'menu_position'         => 5,
// 		'show_in_admin_bar'     => true,
// 		'show_in_nav_menus'     => true,
// 		'can_export'            => true,
// 		'has_archive'           => true,
// 		'exclude_from_search'   => false,
// 		'publicly_queryable'    => true,
// 		'capability_type'       => 'page',
// 	);
// 	register_post_type( 'event', $args );
//
// }
// add_action( 'init', 'custom_post_type', 0 );
